package crossBrowserTest;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.DataProvider;
import com.saucelabs.common.SauceOnDemandAuthentication; 
 import com.saucelabs.common.SauceOnDemandSessionIdProvider; 
 import com.saucelabs.testng.SauceOnDemandAuthenticationProvider; 
 import com.saucelabs.testng.SauceOnDemandTestListener; 
 import org.openqa.selenium.JavascriptExecutor; 
 import org.openqa.selenium.WebDriver; 
import org.openqa.selenium.remote.CapabilityType; 
 import org.openqa.selenium.remote.DesiredCapabilities; 
 import org.openqa.selenium.remote.RemoteWebDriver; 
 import org.testng.ITestResult; 
 import org.testng.annotations.AfterMethod; 
 import java.lang.reflect.Method; 
 import java.net.MalformedURLException; 
 import java.net.URL; 
 import java.rmi.UnexpectedException; 


public final class Driver {

	private static final String USERNAME = "VishalSelenium";
    private static final String ACCESS_KEY = "71af486d-0eec-4c2f-a33f-a95bfe6beca6";

    public static WebDriver createDriver(String browser, String version, String os) {
        // Should probably validate the arguments here
        try {
            DesiredCapabilities capabilities = new DesiredCapabilities();
            capabilities.setCapability(CapabilityType.BROWSER_NAME, browser);
            capabilities.setCapability(CapabilityType.VERSION, version);
            capabilities.setCapability(CapabilityType.PLATFORM, os);
            return new RemoteWebDriver(new URL("http://" + USERNAME + ":" + ACCESS_KEY + "@ondemand.saucelabs.com:80/wd/hub"),
                                       capabilities);
        } catch (MalformedURLException e) {
            throw new RuntimeException("Failure forming the URL to create a web driver", e);
        }
    }

    private Driver() {
        throw new AssertionError("This is a static class and shouldn't be instantiated.");
    }



}
