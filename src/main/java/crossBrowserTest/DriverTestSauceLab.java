package crossBrowserTest;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class DriverTestSauceLab {

	@DataProvider(name = "DriverInfoProvider", parallel = true) // data providers force single threaded by default
	public Object[][] driverInfoProvider() {
		return new Object[][] { { "firefox", "53.0", "Windows 10" }, { "chrome", "61.0", "Windows 10" },
				
		};
	}

	@Test(dataProvider = "DriverInfoProvider")
	public void testFoo(String browser, String version, String os) throws InterruptedException {
		WebDriver driver = Driver.createDriver(browser, version, os);
		try {
			// simple output to see the thread for each test method instance
			System.out.println("starting test in thread: " + Thread.currentThread().getName());
			// Putting this in a try catch block because you want to be sure to close the
			// driver to free
			// up the resources even if the test fails
			driver.get("https://amazon.com");
			driver.manage().window().maximize();
			
		
			driver.findElement(By.linkText("Today's Deals")).click();
		     Thread.sleep(2000);
		
		    driver.findElement(By.linkText("Today's Deals")).click();

			
			
			
			Thread.sleep(4000);
		} finally {
			driver.quit();
		}

	}
}
