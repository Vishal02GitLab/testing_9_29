package crossBrowserTest;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import java.net.URL;

public class SauceLabDemo {

	public static final String USERNAME = "VishalSelenium";

	  public static final String ACCESS_KEY = "71af486d-0eec-4c2f-a33f-a95bfe6beca6";

	  public static final String URL = "https://" + USERNAME + ":" + ACCESS_KEY + "@ondemand.saucelabs.com:443/wd/hub";

	 

	  public static void main(String[] args) throws Exception {

	 

	    DesiredCapabilities caps = DesiredCapabilities.chrome();

	    caps.setCapability("platform", "Windows 10");

	    caps.setCapability("version", "43.0");

	 

	    WebDriver driver = new RemoteWebDriver(new URL(URL), caps);

	 

	    /**

	     * Goes to Sauce Lab's guinea-pig page and prints title

	     */

	 

	    driver.get("https://www.amazon.com");

	    System.out.println("title of page is: " + driver.getTitle());

	 

	    driver.quit();

	  }
}
