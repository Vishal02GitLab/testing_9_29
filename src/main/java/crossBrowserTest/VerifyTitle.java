package crossBrowserTest;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class VerifyTitle {

	WebDriver driver;

	@BeforeMethod
	public void beforeMethod() {
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\chromedriver.exe");
		//System.setProperty("webdriver.gecko.driver", "C:\\Users\\visagarw\\Downloads\\geckodriver.exe");
		//System.setProperty("webdriver.ie.driver", System.getProperty("user.dir")+ "\\IEDriverServer.exe");

	}

	@Test
	public void testChromeBrowser() {
		
		//System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		try {
			
			driver.get("http://localhost:8081");
			driver.manage().window().maximize();

			WebElement enterId = driver.findElement(By.name("j_username"));
			enterId.sendKeys("Vishal02");

			WebElement enterPass = driver.findElement(By.name("j_password"));
			enterPass.sendKeys("Vishal8!");
			driver.findElement(By.id("yui-gen1-button")).click();

			if (driver instanceof TakesScreenshot) {
				TakesScreenshot screenShorter = (TakesScreenshot) driver;
				File outputFile = screenShorter.getScreenshotAs(OutputType.FILE);
				FileUtils.copyFile(outputFile, new File("c:\\\\vishal\\\\screenshot.png"));

				driver.findElement(By.xpath("//*[@id=\"header\"]/div[2]/span/a[2]/b")).click();

			}
			Thread.sleep(6000);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			driver.close();
			driver.quit();

		}
	}

	@Test
	public void testFireFoxBrowser() throws InterruptedException {
		//System.setProperty("webdriver.gecko.driver", "C:\\Users\\visagarw\\Downloads\\geckodriver.exe");
		driver = new FirefoxDriver();
		System.out.println("hiiii");
		driver.manage().window().maximize();
		try {
			driver.get("https://www.google.com");
			driver.manage().window().maximize();

			WebElement element = driver.findElement(By.xpath("//*[@id=\"lst-ib\"]"));
			
			System.out.println(element);
			element.sendKeys("selenium");

			driver.findElement(By.name("btnK")).click();

			Thread.sleep(6000);

		} catch (WebDriverException e) {

			e.printStackTrace();
		} finally {
			driver.close();
			driver.quit();
		}
	}

	
	/* @Test 
	  public void testMethodsThree() { 
		  driver=new InternetExplorerDriver();
	 
	  driver.manage().window().maximize();
	  
	  driver.get("http://www.Amazon.com");
	  
	  driver.quit(); 
	 }*/
	 

}
