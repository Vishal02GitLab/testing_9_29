package crossBrowserTest;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class ParallelExecution {

	WebDriver driver;
	
	@Test
	@Parameters("browser")
	public void verifyParallelExecution(String browserName) throws InterruptedException {
		
		    if(browserName.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", "C:\\Users\\visagarw\\Downloads\\geckodriver.exe");
			driver = new FirefoxDriver();
			System.out.println("hiiii firefox");
			try {
				driver.get("https://www.google.com");
				driver.manage().window().maximize();

				WebElement element = driver.findElement(By.xpath("//*[@id=\"lst-ib\"]"));
				
				System.out.println(element);
				element.sendKeys("selenium");

				driver.findElement(By.name("btnK")).click();

				Thread.sleep(4000);

			} catch (WebDriverException e) {

				e.printStackTrace();
			} finally {
				driver.close();
				driver.quit();
			}
		}
		    else if(browserName.equalsIgnoreCase("chrome")) {
				 System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+ "\\chromedriver.exe");
				driver  = new ChromeDriver();
				driver.get("http://localhost:8081");
				System.out.println("hiiii chrome");
				driver.manage().window().maximize();
				try {
					
					driver.get("http://localhost:8081");
					driver.manage().window().maximize();

					WebElement enterId = driver.findElement(By.name("j_username"));
					enterId.sendKeys("Vishal02");

					WebElement enterPass = driver.findElement(By.name("j_password"));
					enterPass.sendKeys("Vishal8!");
					driver.findElement(By.id("yui-gen1-button")).click();

					if (driver instanceof TakesScreenshot) {
						TakesScreenshot screenShorter = (TakesScreenshot) driver;
						File outputFile = screenShorter.getScreenshotAs(OutputType.FILE);
						FileUtils.copyFile(outputFile, new File("c:\\\\vishal\\\\screenshot.png"));

						driver.findElement(By.xpath("//*[@id=\"header\"]/div[2]/span/a[2]/b")).click();

					}
					Thread.sleep(6000);
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					driver.close();
					driver.quit();
					
				}
				
			}
			else if(browserName.equalsIgnoreCase("IE")) {
				System.setProperty("webdriver.ie.driver", System.getProperty("user.dir")+ "\\IEDriverServer.exe");
				driver  = new InternetExplorerDriver();	
				driver.get("https://www.t-mobile.com");
				System.out.println("hiiii i.e.");
			//	driver.manage().window().maximize();
				System.out.println(driver.getTitle());	
				driver.quit();
			}
	}
}
