package dataDrivenTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class ParameterFromTestNG {

	WebDriver driver;
    @Test
    @Parameters({"author","searchKey"})
    public void testParameterWithXML( @Optional("Abc") String author,String searchKey) throws InterruptedException, StaleElementReferenceException{
    	
    	 System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+ "\\chromedriver.exe");
    	 
    	driver  = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        try {
        driver.get("https://google.com");
        driver.manage().window().maximize();
        
        WebElement searchText = driver.findElement(By.name("q"));
        //Searching text in google text box
        searchText.sendKeys(searchKey);

        WebElement elem = driver.findElement(By.name("btnK"));
         elem.submit();  
        System.out.println("Welcome ->"+author+" Your search key is->"+searchKey);
     
        Thread.sleep(3000);
       //AssertJUnit.assertTrue(searchText.getAttribute("value").equalsIgnoreCase(searchKey));
        } finally {
        	driver.quit();
        }
    }
}
