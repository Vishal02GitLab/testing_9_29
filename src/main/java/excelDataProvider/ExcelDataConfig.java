package excelDataProvider;

import java.io.*;
import java.io.FileInputStream;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelDataConfig {

	XSSFWorkbook wb;
	XSSFSheet sheet1;
	
	public  ExcelDataConfig(String excelPath) {
		try {
			File file = new File(excelPath);
			FileInputStream fis = new FileInputStream(file);
			wb = new XSSFWorkbook(fis);
			
			
		}catch(Exception ex) {
			ex.getMessage();
		}
	}
	public String getData(int sheetNumber, int row, int column) {
		sheet1 = wb.getSheetAt(sheetNumber);
		String data = sheet1.getRow(row).getCell(column).getStringCellValue();
		return data;
			
	}
	public int getRowCount(int sheetIndex) {
		int row = 0;
		wb.getSheetAt(sheetIndex).getLastRowNum();
	     row = row+1;
		return row;
		
	}
}
