package excelDataProvider;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class GitLabLogin {

	WebDriver driver;
	
	@Test(dataProvider = "gitLabData")
	public void loginGitLab(String username, String password) throws InterruptedException
	{
		 System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+ "\\chromedriver.exe");
    	 
	    	driver  = new ChromeDriver();
	        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	        try {
	        driver.get("https://gitlab.com/users/sign_in");
	        driver.manage().window().maximize();
	        
	        driver.findElement(By.id("user_login")).sendKeys(username);
	        driver.findElement(By.id("user_password")).sendKeys(password);
	        driver.findElement(By.xpath("//*[@id=\"new_user\"]/div[4]/input")).click();
	        Thread.sleep(3000);
	        System.out.println("User Login");
	        
	        
	        }finally {
	        	driver.quit();
	        }
	}
	
	@DataProvider(name="gitLabData")
    public Object[][] getDataFromDataprovider(){
		
		ExcelDataConfig excel = new ExcelDataConfig("C:\\Users\\visagarw\\eclipse-working\\TestNGtesting\\src\\TestData\\excelData.xlsx");
		// getRowCount(0) taking 0 argument as it is sheet 0.
		int rows = excel.getRowCount(0);
		Object[][] data = new Object[rows][2];
		
		for(int i=0;i<rows;i++) {
			
			data[i][0] = excel.getData(0, i, 0) ;
			data[i][1] = excel.getData(0, i, 1) ;
			
		}
    return data ;
	}
}
